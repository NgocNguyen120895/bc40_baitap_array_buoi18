
var numbArr = [];

// Function to add elements to the array.
function addToArr() {
    var number = parseInt(document.getElementById('txt-array').value);
    document.getElementById('txt-array').value = "";
    var result = numbArr.push(number);
    console.log("Arr hien tai: ", result);
    document.querySelector("#result").innerHTML = `Current array: ${numbArr}` //Display current array.
}

//Eventlistener is calling addToArr by using addEventListener
var addNumberToArr = document.getElementById("btn-add-array");
addNumberToArr.addEventListener("click", addToArr);

//Eventlistener is calling tongSoDuong() to calculate sum of the positive number in the numArr array.
var mySumPos = document.getElementById("tinhtongbtn");
mySumPos.addEventListener("click", function () {
    tongSoDuong(numbArr);
});

//Eventlistener is calling demSoDuong() to count the positive numbers in the numArr array.
var myCountPos = document.getElementById("demSoDuong");
myCountPos.addEventListener("click", function () {
    demSoDuong(numbArr);
});

//
var myFindMin = document.getElementById("timSoNhoNhat").addEventListener("click", function () {
    soNhoNhat(numbArr);
});

var myFindPosMin = document.getElementById("timSoDuongNhoNhat").addEventListener("click", function () {
    timSoDuongNhoNhat(numbArr);
});

document.getElementById("timSoChan").addEventListener("click", function () {
    timSoChan(numbArr);
});

// console.log("find even ", findEven);

function tongSoDuong(x) {
    var sumPos = 0;
    for (var i = 0; i < x.length; i++) {
        if (x[i] > 0) {
            sumPos = sumPos + x[i];
        }
    }
    // console.log("tong so duong la: ", sumPos)
    document.getElementById("result-pos").innerHTML = `Tổng các số dương:  ${sumPos}`;

}

function demSoDuong(x) {
    var countPos = 0;
    for (var i = 0; i < x.length; i++) {
        if (x[i] > 0) {
            countPos++;
        }
    }
    document.getElementById("re-countPos").innerHTML = `Số lượng số dương là:  ${countPos}`;

}

function soNhoNhat(x) {
    var minArr;
    minArr = Math.min(...x);
    document.getElementById("re-min").innerHTML = `Số nhỏ nhất: ${minArr}`;
}

function timSoDuongNhoNhat(x) {
    var minPosArr = [];
    for (var i = 0; i < x.length; i++) {
        if (x[i] > 0) {
            minPosArr.push(x[i]);
        }
    }
    if (minPosArr.length > 0) {
        var min = Math.min(...minPosArr);
        document.getElementById("re-min-pos").innerHTML = `Số dương nhỏ nhất: ${min}`;
    } else {
        document.getElementById("re-min-pos").innerHTML = `ko co so duong`;
    }
}

function timSoChan(x) {
    var findEven;
    for (var i = 0; i < x.length; i++) {
        if (x[i] % 2 == 0) {
            findEven = x[i];
        }
        document.getElementById('re-find-even').innerHTML = `Số chẵn cuối cùng là: ${findEven}`;
    }
}

function swapingArr() {
    var i1 = document.getElementById('index-1').value * 1;
    var i2 = document.getElementById('index-2').value * 1;
    var temp;
    temp = numbArr[i1];
    numbArr[i1] = numbArr[i2];
    numbArr[i2] = temp;
    document.getElementById('result-swap').innerHTML = `${numbArr}`;
}

var sapXep = document.getElementById('sort');
sapXep.addEventListener('click', function () {
    sapXepTangDan();
    document.getElementById('sort-result').innerHTML = `${numbArr}`;
})
function sapXepTangDan() {
    numbArr.sort(function (a, b) { return a - b });
}


